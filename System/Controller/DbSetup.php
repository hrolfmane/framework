<?php


namespace System\Controller;


use Model\UserModel;
use System\ErrorHandler\ErrorHandler;
use System\Model\DatabaseModel;
use System\Model\Model;
use System\ResponseType\Block;
use System\ResponseType\InternalRedirect;
use System\View\View;

class DbSetup extends Controller
{
    public function __invoke()
    {
        if(DatabaseModel::ConnectionIsValid())return new Block();
        if($this->request->GetMethod()=="POST"){
            $this->request->AssertVar("host","host is required");
            $this->request->AssertVar("port","host is required");
            $this->request->AssertVar("db","host is required");
            $this->request->AssertVar("user","host is required");
            if(!ErrorHandler::IsUserError()){
                DatabaseModel::SetConnectionInfo($this->request->host,$this->request->db,
                    $this->request->user,$this->request->pass,$this->request->port);
                Model::Init();
                if(!DatabaseModel::ConnectionIsValid()) {
                    ErrorHandler::AddUserError(DatabaseModel::LastTryError());
                }else{
                    return new InternalRedirect("/");
                }
            }
        }
        session_unset();
        return new View("setup.php");
    }
}