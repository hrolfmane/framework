<?php


namespace System\Controller;


use System\ResponseType\File;
use System\View\View;

class FileController extends Controller
{
    public function __invoke()
    {
        $file = new File($this->request->GetUri());
        return $file;
    }
}