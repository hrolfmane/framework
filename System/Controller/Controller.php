<?php


namespace System\Controller;


use System\ErrorHandler\ErrorHandler;
use System\Request\Request;
use System\ResponseType\Block;

class Controller
{
    /**
     * @var Request
     */
    protected $request=null;
    public function handle(string $function,Request $request,array $routeParams)
    {
        $this->request = $request;
        $reflection = new \ReflectionClass($this);
        if($function=="")$function="__invoke";
        if ($reflection->hasMethod($function)) {
            try {
                return $reflection->getMethod($function)->invokeArgs($this, $routeParams);
            } catch (\Exception $e) {
                ErrorHandler::AddSystemError($e->getMessage(), 2);
                return new Block();
            }
        } else {
            ErrorHandler::AddSystemError($function." controller function not found", 2);
            return new Block();
        }
    }
}