<?php


namespace System\Registry;


use System\Controller\Controller;
use System\ErrorHandler\ErrorHandler;
use System\Middleware\Middleware;

class Registry
{
    public static function GetController(string $name): ?Controller{
        $systemController="\\System\\Controller\\".$name;
        $userController="\\Controller\\".$name;
        try {
            return new $userController();
        }catch (\Error $e){};
        try {
            return new $systemController();
        }catch (\Error $e){
            ErrorHandler::AddSystemError("cant resolve controller: ".$name." in namespaces: ".$systemController." ,".$userController,1);
            return null;
        }
    }
    public static function GetMiddleware(string $name): ?Middleware{
        $systemMiddleware="\\System\\Middleware\\".$name;
        $userMiddleware="\\Middleware\\".$name;
        try {
            return new $userMiddleware();
        }catch (\Error $e){};
        try {
            return new $systemMiddleware();
        }catch (\Error $e){
            ErrorHandler::AddSystemError("cant resolve middleware: ".$name,1);
            return null;
        }
    }
}