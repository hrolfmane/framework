<?php


namespace System\ResponseType;


class InternalRedirect implements ResponseType
{
    /**
     * @var string
     */
    private $redirect="";
    public function __construct(string $redirect)
    {
        $this->redirect=$redirect;
    }

    public function GetStatus(): int
    {
        return -2;
    }

    public function GetResponse(): string
    {
        return $this->redirect;
    }
}