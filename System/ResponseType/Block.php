<?php


namespace System\ResponseType;


class Block  implements ResponseType
{
    public function GetStatus(): int
    {
        return -1;
    }

    public function GetResponse(): string
    {
        return "";
    }
}