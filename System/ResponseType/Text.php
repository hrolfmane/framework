<?php


namespace System\ResponseType;


class Text implements ResponseType
{
    /**
     * @var string
     */
    private $text="";
    /**
     * @var string
     */
    private $color="";
    public function __construct(string $text,string $color="black")
    {
        $this->text=$text;
        $this->color=$color;
    }

    public function GetStatus(): int
    {
        return 0;
    }

    public function GetResponse(): string
    {
        ob_start();
        ?>
            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="utf-8">
                <title></title>
            </head>
            <body>
            <span style="color:<?= $this->color ?>"><?= $this->text ?></span>
            </body>
            </html>
        <?php
        return ob_get_clean();
    }
}