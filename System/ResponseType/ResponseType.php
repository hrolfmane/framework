<?php


namespace System\ResponseType;


interface ResponseType
{
    public function GetStatus() : int;
    public function GetResponse() : string;
}