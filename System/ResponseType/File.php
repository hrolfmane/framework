<?php


namespace System\ResponseType;

use System\ErrorHandler\ErrorHandler;

class File implements ResponseType
{

    /**
     * @var string
     */
    private $file="";
    /**
     * @var bool
     */
    private $valid=true;
    /**
     * @var int
     */
    private $status=0;

    public function __construct(string $name)
    {
        if($name!="") {
            if ($name[0] == "/") $name = substr($name, 1);
            $this->file = $_SERVER["DOCUMENT_ROOT"] . "/Storage/" . $name;
            if (!($this->valid = (file_exists($this->file)&&(!is_dir($this->file))))) {
                $this->status = -1;
                ErrorHandler::AddSystemError("FileNotFound: ".$this->file,1);
            }
        }else{$this->valid=false;$this->status=-1;}
    }

    public function isValid():bool{
        return $this->valid;
    }
    public function GetStatus(): int
    {
        return $this->status;
    }

    public function GetResponse(): string
    {
        if($this->valid) {
            header("Cache-Control: public");
            header("Content-Type:" . $this->get_mime_type($this->file));
            header("Content-Transfer-Encoding: Binary");
            header("Content-Length:" . filesize($this->file));
            header("Content-Disposition: inline; filename=" . basename($this->file));
            readfile($this->file);
            die();
        }else{
            return "something went wrong";
        }
    }

    //this isn't my code
    private function get_mime_type($filename):string
    {
        $idx = explode('.', $filename);
        $idx = end($idx);

        $mimet = array(
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',
            'docx' => 'application/msword',
            'xlsx' => 'application/vnd.ms-excel',
            'pptx' => 'application/vnd.ms-powerpoint',


            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        if (array_key_exists($idx, $mimet)) {
            return $mimet[$idx];
        } else {
            return 'application/octet-stream';
        }
    }
}