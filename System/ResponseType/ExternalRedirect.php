<?php


namespace System\ResponseType;


class ExternalRedirect implements ResponseType
{
    /**
     * @var string
     */
    private $redirect = "";

    public function __construct(string $redirect)
    {
        $this->redirect = $redirect;
    }

    public function GetStatus(): int
    {
        return -3;
    }

    public function GetResponse(): string
    {
        header("Location: ".$this->redirect);
        die();
    }
}