<?php


namespace System\Model;


use System\ErrorHandler\ErrorHandler;

class DataPacket
{
    /**
     * @var string
     */
    private $key="def";
    /**
     * @var bool
     */
    private $dirty=false;
    /**
     * @var array
     */
    private $data = array();
    /**
     * @var bool
     */
    private $toDrive = false;

    /**
     * @var DataPacket[]
     */
    private static $driveKeys=array();

    /**
     * @var DataPacket[]
     */
    private static $sessionKeys=array();

    public static function getDataPacket(string $key,bool $toDrive=false):DataPacket{
        if($toDrive){
            if(isset(DataPacket::$driveKeys[$key]))return DataPacket::$driveKeys[$key];
            $newKey=New DataPacket($key,$toDrive);
            DataPacket::$driveKeys[$key]=$newKey;
            return $newKey;
        }else{
            if(isset(DataPacket::$sessionKeys[$key]))return DataPacket::$sessionKeys[$key];
            $newKey=New DataPacket($key,$toDrive);
            DataPacket::$sessionKeys[$key]=$newKey;
            return $newKey;
        }
    }

    public function __set($name, $value)
    {
        if(($this->data[$name]??NULL)!==$value) {
            if ($value === NULL) unset($this->data[$name]);
            else {
                $this->data[$name] = $value;
            }
            $this->dirty = true;
        }
    }
    public function __get($name)
    {
        return $this->data[$name]??NULL;
    }

    public function isOnDrive(){
        return $this->toDrive;
    }
    public function clearDataPacket(){
        foreach ($this->data as $key => $val){
            unset($this->data[$key]);
        }
    }
    public function isClear():bool{
        return (count($this->data)==0);
    }
    private function __construct(string $key,bool $toDrive=false)
    {
        if($key!="")$this->key=$key;
        $this->toDrive=$toDrive;
        if($this->toDrive){
            $realFileName=$_SERVER['DOCUMENT_ROOT']."/System/Model/ModelStorage/".$this->key;
            if(is_file($realFileName)){
                $file=file_get_contents($realFileName);
                if($file) {
                    $this->data = json_decode($file,true);
                }else{
                    ErrorHandler::AddSystemError("file reading failed:".$realFileName,1);
                }
            }
        }else{
            if(!isset($_SESSION[$this->key])) $_SESSION[$this->key]=array();
             $this->data= &$_SESSION[$this->key];

        }
    }
    public function __destruct()
    {
        if($this->dirty){
            if($this->toDrive){
                $realFileName=$_SERVER['DOCUMENT_ROOT']."/System/Model/ModelStorage/".$this->key;
                if(count($this->data)>0) {
                    if(!file_put_contents($realFileName, json_encode($this->data))){
                        ErrorHandler::AddSystemError("file writing failed:".$realFileName,1);
                    }
                }else @unlink($realFileName);
            }else{
                if(count($this->data)==0)unset($_SESSION[$this->key]);
            }
        }
    }
}