<?php


namespace System\Model;


use PDO;
use System\ErrorHandler\ErrorHandler;

class Model
{
    /**
     * @var \PDO
     */
    private static $PDO=null;
    public static function Init():bool{
        if(DatabaseModel::IsConnectionInfo()) {
            try {
                Model::$PDO = new PDO(DatabaseModel::GetConnectionString(), DatabaseModel::GetUsername(), DatabaseModel::GetPassword());
                Model::$PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                DatabaseModel::ValidateConnection();
                return true;
            } catch (\Exception $e) {
                DatabaseModel::SetLastTryError($e->getMessage());
                ErrorHandler::AddSystemError("Database error: " . $e->getMessage(), 0);
                return false;
            }
        }else{
            ErrorHandler::AddSystemError("Database connection info not set", 0);
            return false;
        }
    }
    protected static function GetDB(){
        return Model::$PDO;
    }
    public static function ExPrepStmFA(string $stm,array $vars):?array{
        try {
            $stm = Model::$PDO->prepare($stm);
            $stm->execute($vars);
            return $stm->fetchAll();
        }catch (\Exception $e){
            ErrorHandler::AddSystemError("Database error: ".$e->getMessage(),1);
            return null;
        }
    }
    public static function ExPrepStmRC(string $stm,array $vars):?int{
        try {
            $stm = Model::$PDO->prepare($stm);
            $stm->execute($vars);
            return $stm->rowCount();
        }catch (\Exception $e){
            ErrorHandler::AddSystemError("Database error: ".$e->getMessage(),1);
            return null;
        }
    }
}