<?php


namespace System\Model;


class DatabaseModel
{
    /**
     * @var DataPacket
     */
    private static $connectionData = null;
    private static function InitData(){
        if(DatabaseModel::$connectionData == null) {
            DatabaseModel::$connectionData = DataPacket::getDataPacket("connectionData", true);
        }
    }
    public static function ConnectionIsValid():bool{
        DatabaseModel::InitData();
       return  DatabaseModel::$connectionData->lastTry == "successful";
    }
    public static function ValidateConnection(){
        DatabaseModel::InitData();
        DatabaseModel::$connectionData->lastTry="successful";
        DatabaseModel::$connectionData->lastTryError="";
    }
    public static function LastTryError():string{
        DatabaseModel::InitData();
        return DatabaseModel::$connectionData->lastTryError??"";
    }
    public static function SetLastTryError(string $error){
        DatabaseModel::InitData();
        DatabaseModel::$connectionData->lastTryError=$error;
    }
    public static function SetConnectionInfo(string $host,string $db,string $user,string $pass,string $port="3306"){
        DatabaseModel::InitData();
        DatabaseModel::$connectionData->host=$host;
        DatabaseModel::$connectionData->db=$db;
        DatabaseModel::$connectionData->user=$user;
        DatabaseModel::$connectionData->pass=$pass;
        DatabaseModel::$connectionData->port=$port;
        DatabaseModel::$connectionData->isSet=true;
    }
    public static function GetConnectionString():string{
        DatabaseModel::InitData();
        return 'mysql:host=' . (DatabaseModel::$connectionData->host??"") . ';dbname=' .
            (DatabaseModel::$connectionData->db??"") . ';port=' . (DatabaseModel::$connectionData->port??"") . ';charset=utf8';
    }
    public static function GetUsername():string{
        DatabaseModel::InitData();
        return (DatabaseModel::$connectionData->user??"");
    }
    public static function GetPassword():string{
        DatabaseModel::InitData();
        return (DatabaseModel::$connectionData->pass??"");
    }
    public static function IsConnectionInfo():bool{
        DatabaseModel::InitData();
        return DatabaseModel::$connectionData->isSet == true;
    }

}