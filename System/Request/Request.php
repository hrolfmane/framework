<?php


namespace System\Request;


use System\ErrorHandler\ErrorHandler;

class Request
{
    /**
     * @var array|null
     */
    private $vars = null;
    /**
     * @var string
     */
    private $method = "";
    /**
     * @var string
     */
    private $uri = "";

    public static function BuildRequest():Request{
        $uri=$_SERVER['REQUEST_URI'];
        $method=$_SERVER['REQUEST_METHOD'];
        $vars=array_merge($_POST,$_GET);
        foreach($vars as $name=>$val){
            $vars[$name]=htmlspecialchars($val,ENT_HTML5);
        }
        return new Request($vars,$uri,$method);
    }

    public function __construct(array $vars,string $url,string $method)
    {
        $this->vars=$vars;
        $this->uri=$url;
        $this->method=$method;
    }

    public function __get($name)
    {
        if(isset($this->vars[$name])){
            return $this->vars[$name];
        }else return "";
    }

    public function GetAllVars(): array{
        return $this->vars;
    }

    public function GetMethod():string{
        return $this->method;
    }

    public function GetUri():string{
        return $this->uri;
    }
    public function RedirectUrl(string $url){
        $this->uri=$url;
    }
    public function AssertVar(string $name,string $errorStr):bool{
        if(!isset($this->vars[$name])){
            ErrorHandler::AddUserError($errorStr);
            return false;
        }
        return true;
    }
    public function AssertVarFormat(string $name,string $format,string $errorStr):bool{
        if(isset($this->vars[$name])){
            if (!preg_match("/" . $format . "/", $this->vars[$name])) {
                ErrorHandler::AddUserError($errorStr);
                return false;
            }
            return true;
        }
        return false;
    }
    public function AssertVarEquality(string $name1,string $name2,string $errorStr):bool{
        if(isset($this->vars[$name1])&&isset($this->vars[$name2])){
            if($this->vars[$name1]!=$this->vars[$name2]){
                ErrorHandler::AddUserError($errorStr);
                return false;
            }
            return true;
        }
        return false;
    }
}