<?php


namespace System\Middleware;

use Closure;
use System\Request\Request;


interface Middleware
{
    public function handle(Request $request,Closure $next);
}