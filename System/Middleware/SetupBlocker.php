<?php


namespace System\Middleware;


use Closure;
use System\Model\DatabaseModel;
use System\Request\Request;
use System\ResponseType\InternalRedirect;

class SetupBlocker implements Middleware
{

    public function handle(Request $request, Closure $next)
    {
        if(DatabaseModel::ConnectionIsValid())return $next($request);
        return new InternalRedirect("/setup");
    }
}