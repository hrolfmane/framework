<?php

namespace System\Autoloader;


class Autoloader
{
    /**
     * @var bool
     */
    private static $registered=false;
    public static function RegisterHandler(){
        if(!(Autoloader::$registered)){
            Autoloader::$registered=true;
            spl_autoload_register(function ($class_name) {
                $path=$_SERVER['DOCUMENT_ROOT']."/".str_replace("\\","/",$class_name).".php";
                $path=str_replace("//","/",$path);
                if(is_file($path)) require ($path);
            });
        }
    }
}