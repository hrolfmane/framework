<?php


namespace System\ErrorHandler;


class ErrorHandler
{
    /**
     * @var string[]
     */
    private static $userError=array();
    /**
     * @var string[]
     */
    private static $systemError=array();
    public static function AddUserError(string $error){
        if($error!="") {
            if (!in_array($error, ErrorHandler::$userError)) {
                array_push(ErrorHandler::$userError, $error);
            }
        }
    }
    public static function AddSystemError(string $error,int $out=1){
        if($error!="") {
            $debug = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, $out + 1);
            $data = "";
            if (isset($debug[1])) {
                $data .= " [in file: " . $debug[$out]["file"] . ", line: " . $debug[$out]["line"] . "  ]";
            }
            array_push(ErrorHandler::$systemError, $error . $data);
        }
    }
    public static function RegisterHandler(){
        set_error_handler ( function(int $errno,string $errstr,string $errfile,int $errline){
            ErrorHandler::AddSystemError("PHP error: ".$errno." ".$errstr." in file ".$errfile." in line ".$errline);
        }, E_ALL );
    }
    public static function GetSystemError():array{
        return ErrorHandler::$systemError;
    }
    public static function GetUserError():array{
        return ErrorHandler::$userError;
    }
    public static function IsUserError():bool{
        return count(ErrorHandler::$userError)>0;
    }
    public static function IsSystemError():bool{
        return count(ErrorHandler::$systemError)>0;
    }
    public static function printSystemError(){
        foreach (ErrorHandler::$systemError as $er){
            ?>
            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="utf-8">
                <title></title>
            </head>
            <body style='background-color: #1d2124; color: #c69500;font-family: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New";', monospace">
            <?php
            echo $er;
            echo "<br>";
            ?>
            </body>
            </html>
            <?php
        }
    }
}