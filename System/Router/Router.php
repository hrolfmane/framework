<?php


namespace System\Router;


use System\ErrorHandler\ErrorHandler;
use System\Request\Request;
use System\ResponseType\InternalRedirect;

class Router
{
    /**
     * @var Route[]
     */
    private $routes=array();
    /**
     * @var Route
     */
    private $default=null;
    /**
     * @var array
     */
    private $middlewareGroup=array();

    private static function mergeArrayOfArrays(array $arr2):array{
        $ret=array();
        foreach($arr2 as $element){
            if(is_array($element)){
                array_push($ret,...$element);
            }else{
                array_push($ret,$element);
            };
        }
        return $ret;
    }

    public function Def($toCall):?Route{
        if(is_callable($toCall)){
            $this->default=new Route("",array());
            $this->default->SetFunction($toCall);
            return $this->default->AddMiddleware(...Router::mergeArrayOfArrays($this->middlewareGroup));
        }else if(is_string($toCall)){
            $this->default=new Route("",array());
            $this->default->SetController($toCall);
            return $this->default->AddMiddleware(...Router::mergeArrayOfArrays($this->middlewareGroup));
        }else{
            ErrorHandler::AddSystemError("incorrect router bind call",2);
            return null;
        }
    }
    public function __call($name, $arguments) : ?Route
    {
        $functions=array("post","get","head","put","delete","connect","options","trace","patch");

        $methods=array();
        if($name="all"){
            foreach($functions as $s){
                array_push($methods,strtoupper($s));
            }
        }else{
            foreach($functions as $s){
                if(strpos($name,$s)!==false){
                    array_push($methods,strtoupper($s));
                }
            }
        }

        if(!is_string($arguments[0])){
            ErrorHandler::AddSystemError("incorrect router bind call",2);
            return null;
        }
        if(is_callable($arguments[1])){
            array_push($this->routes,new Route($arguments[0],$methods));
            $this->routes[count($this->routes)-1]->SetFunction($arguments[1]);
            return $this->routes[count($this->routes)-1]->AddMiddleware(...Router::mergeArrayOfArrays($this->middlewareGroup));
        }else if(is_string($arguments[1])){
            array_push($this->routes,new Route($arguments[0],$methods));
            $this->routes[count($this->routes)-1]->SetController($arguments[1]);
            return $this->routes[count($this->routes)-1]->AddMiddleware(...Router::mergeArrayOfArrays($this->middlewareGroup));
        }else{
            ErrorHandler::AddSystemError("incorrect router bind call",2);
            return null;
        }
    }
    public function Handle(Request $request){
        $ret=new InternalRedirect($request->GetUri());
        $limiter=0;
        while($ret!=null&&$ret->GetStatus()==-2){
            if($limiter>20){$ret=null;break;}
            $request->RedirectUrl($ret->GetResponse());
            $ret=null;
            foreach($this->routes as $route){
                if($route->Matches($request)){
                    $ret=$route->Activate();
                    break;
                }
            }
        }
        if($ret==null||$ret->GetStatus()==-1){
            $this->default->SetRequest($request);
            return $this->default->Activate();
        }
        return $ret;
    }
    public function StartMiddlewareGroup(string ...$mids){
        array_push($this->middlewareGroup,$mids);
    }
    public function EndMiddlewareGroup(){
        if(count($this->middlewareGroup)>0)
        array_pop($this->middlewareGroup);
    }
}