<?php


namespace System\Router;


use System\ErrorHandler\ErrorHandler;
use System\Registry\Registry;
use System\Request\Request;
use System\ResponseType\Block;
use System\ResponseType\ResponseType;

class Route
{
    /**
     * @var bool
     */
    private $useFunction = false;
    /**
     * @var string
     */
    private $functionToCall = null;

    /**
     * @var string
     */
    private $controllerToUse = null;
    /**
     * @var string
     */
    private $controllerFunctionToCall = null;

    /**
     * @var string[]
     */
    private $middleWareArray = array();

    /**
     * @var string
     */
    private $route="";
    /**
     * @var string[]
     */
    private $method=array();

    /**
     * @var mixed[]
     */
    private $routeParam=array();
    /**
     * @var Request
     */
    private $request=null;

    public function __construct(string $route,array $method)
    {
        $this->route="/^\\/".str_replace('/','\\/',$route)."$/";
        $this->method=$method;
    }
    public function SetRouteParams(array $param){
        $this->routeParam=$param;
    }
    public function SetRequest(Request $request){
        $this->request=$request;
    }
    public function Matches(Request $request):bool{
        if(in_array($request->GetMethod(),$this->method))
        if(preg_match($this->route,$request->GetUri(),$matches)){
            $this->routeParam=array_slice($matches,1);
            $this->request=$request;
            return true;
        }
        return false;
    }
    public function Activate():ResponseType{
        $call=null;
        if($this->useFunction){
            $call = function (Request $request){
                call_user_func_array($this->functionToCall,$this->routeParam);
            };
        }else {
            $call = function (Request $request){
                $controllerToUseInstance=Registry::GetController($this->controllerToUse);
                if($controllerToUseInstance!=null) return $controllerToUseInstance->handle($this->controllerFunctionToCall, $request, $this->routeParam);
                else{
                    ErrorHandler::AddSystemError($this->controllerToUse." "."controller not found",3);
                    return new Block();
                }
            };
        }
        foreach($this->middleWareArray as $middleware){
            $middlewareInstance=Registry::GetMiddleware($middleware);
            if($middlewareInstance==null){
                ErrorHandler::AddSystemError($middleware." "."middleware not found",3);
                continue;
            }
            $call=function(Request $request) use ($call,$middlewareInstance){
                return $middlewareInstance->handle($request,$call);
            };
        }
        unset($middleware);
        return $call($this->request);
    }

    public function SetFunction(callable $func){
        $this->useFunction=true;
        $this->functionToCall=$func;
    }
    public function SetController(string $controller){
        $this->useFunction=false;
        if(strlen($controller)>0) {
            $cf = explode("@", $controller);
            $this->controllerToUse = $cf[0];
            if (isset($cf[1])) {
                $this->controllerFunctionToCall = $cf[1];
            }else{
                $this->controllerFunctionToCall="";
            }
        }
    }
    public function AddMiddleware(string ...$middlewareList):Route{
        foreach ($middlewareList as $middleware)
        if(!in_array($middleware,$this->middleWareArray)) {
            array_push($this->middleWareArray, $middleware);
        }
        return $this;
    }
}