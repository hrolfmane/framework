<?php


namespace System;

use System\ErrorHandler\ErrorHandler;
use System\Model\Model;
use System\Request\Request;
use System\ResponseType\ResponseType;
use System\Router\Router;

class System
{
    public static function Run():ResponseType{
        Model::Init();
        $router=new Router();
        include $_SERVER['DOCUMENT_ROOT']."/routes.php";
        $router->getpost("setup","DbSetup");
        $router->get("(.+)?","FileController");
        return $router->Handle(Request::BuildRequest());
    }
}