<?php


namespace System\View;
use System\ErrorHandler\ErrorHandler;
use System\ResponseType\ResponseType;

class View implements ResponseType
{
    /**
     * @var array
     */
    private $vars=array();
    /**
     * @var string
     */
    private $file="";
    /**
     * @var int
     */
    private $responseCode=200;
    private $status=0;
    public function __construct(string $filename,int $responseCode=200)
    {
        $realName=$_SERVER['DOCUMENT_ROOT']."/Views/".$filename;
        if(is_file($realName)&&!is_dir($realName)) {
            $this->file = $filename;
            $this->responseCode = $responseCode;
        }else{
            ErrorHandler::AddSystemError("View file: ".$realName." not found",1);
            $this->status=-1;
        }
    }
    public function __set($name, $value):View
    {
        if($name=="multi" && is_array($value)){
            $this->vars=array_merge($this->vars,$value);
        }else {
            $this->vars[$name] = $value;
        }
        return $this;
    }
    public function __get($name)
    {
        if(isset($this->vars[$name])){
            return $this->vars[$name];
        }else{
            return null;
        }
    }

    public function GetResponse():string{
        include_once "ViewFunctions.php";
        http_response_code($this->responseCode);
        $GLOBALS["ViewVars"]=$this->vars;
        $GLOBALS["ViewVars"]["UserError"]=ErrorHandler::GetUserError();
        extract($GLOBALS["ViewVars"]??array());
        ob_start();
            include $_SERVER['DOCUMENT_ROOT']."/Views/".$this->file;
        return ob_get_clean();
    }

    public function GetStatus(): int
    {
        return $this->status;
    }
}