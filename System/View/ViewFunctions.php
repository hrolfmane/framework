<?php
    function makeSingleSlot(string $name){
        if(!isset($GLOBALS["Slots"]))$GLOBALS["Slots"]=array();
        if(isset($GLOBALS["Slots"][$name])){
            if(is_array($GLOBALS["Slots"][$name])){
                $GLOBALS["Slots"][$name][0];
            }else {
                echo $GLOBALS["Slots"][$name];
            }
        }
    }

    function makeMultiSlot(string $name){
        if(!isset($GLOBALS["Slots"]))$GLOBALS["Slots"]=array();
        if(isset($GLOBALS["Slots"][$name])){
            if(is_array($GLOBALS["Slots"][$name])){
                foreach($GLOBALS["Slots"][$name] as $part){
                    echo $part;
                }
            }else {
                echo $GLOBALS["Slots"][$name];
            }
        }
    }

    function bindViewToSlot(string $fileName,string $slotName){
        if(!isset($GLOBALS["Slots"]))$GLOBALS["Slots"]=array();
        ob_start();
            extract($GLOBALS["ViewVars"]??array());
            include $_SERVER['DOCUMENT_ROOT']."/Views/".$fileName;
        $t=ob_get_clean();
        if(isset($GLOBALS["Slots"][$slotName])){
            if(is_array($GLOBALS["Slots"][$slotName])){
                array_push($GLOBALS["Slots"][$slotName],$t);
            }else{
                $GLOBALS["Slots"][$slotName]=array($GLOBALS["Slots"][$slotName],$t);
            }
        }else{
            $GLOBALS["Slots"][$slotName]=$t;
        }
    }
    function startBinding(string $slotName){
        if(!isset($GLOBALS["Slots"]))$GLOBALS["Slots"]=array();
        $GLOBALS["SlotCapture"]=$slotName;
        ob_start();

    }
    function stopBinding(){
        if(!isset($GLOBALS["Slots"]))$GLOBALS["Slots"]=array();
        if(isset($GLOBALS["SlotCapture"])) {
            $t = ob_get_clean();
            if (isset($GLOBALS["Slots"][$GLOBALS["SlotCapture"]])) {
                if (is_array($GLOBALS["Slots"][$GLOBALS["SlotCapture"]])) {
                    array_push($GLOBALS["Slots"][$GLOBALS["SlotCapture"]], $t);
                } else {
                    $GLOBALS["Slots"][$GLOBALS["SlotCapture"]] = array($GLOBALS["Slots"][$GLOBALS["SlotCapture"]], $t);
                }
            } else {
                $GLOBALS["Slots"][$GLOBALS["SlotCapture"]] = $t;
            }
            unset($GLOBALS["SlotCapture"]);
        }
    }
    function includeView(string $name){
        extract($GLOBALS["ViewVars"]??array());
        include $_SERVER['DOCUMENT_ROOT']."/Views/".$name;
    }
    function SetVal(string $name,$val){
        if(!isset($GLOBALS["Slots"]))$GLOBALS["Slots"]=array();
        if(!isset($GLOBALS["Slots"]["Vals"]))$GLOBALS["Slots"]["Vals"]=array();
        $GLOBALS["Slots"]["Vals"][$name]=$val;
    }
    function GetVal(string $name){
        if(!isset($GLOBALS["Slots"]))$GLOBALS["Slots"]=array();
        if(!isset($GLOBALS["Slots"]["Vals"]))$GLOBALS["Slots"]["Vals"]=array();
        return ($GLOBALS["Slots"]["Vals"][$name]??"");
    }