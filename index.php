<?php
    $DEBUG=false;

    session_start();
    include $_SERVER['DOCUMENT_ROOT']."/System/Autoloader/Autoloader.php";
    \System\Autoloader\Autoloader::RegisterHandler();
    \System\ErrorHandler\ErrorHandler::RegisterHandler();
    $response= \System\System::Run();
    if($DEBUG){
        if(\System\ErrorHandler\ErrorHandler::IsSystemError()){
            \System\ErrorHandler\ErrorHandler::printSystemError();
        }
        else{
            echo $response->GetResponse();
        }
    }else{
        echo $response->GetResponse();
    }