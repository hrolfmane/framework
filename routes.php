<?php
        $router->def("NotFound");

        $router->StartMiddlewareGroup("SetupBlocker");

            $router->getpost("","MainPage");
            $router->getpost("logout","UserController@logout")->AddMiddleware("LOutBlock");
            $router->getpost("data","UserController@userdata")->AddMiddleware("LOutBlock");
            $router->getpost("registration","UserController@registration")->AddMiddleware("LInBlock");
            $router->getpost("login","UserController@login")->AddMiddleware("LInBlock");

        $router->EndMiddlewareGroup();