<?php SetVal("title","your data"); ?>
<?php SetVal("active",4); ?>
<?php startBinding("main"); ?>
    <div class="container">
        <div class="row mt-3" >
            <div class="col-lg-3 bg-dark mr-md-3 text-light p-3">
                <?php if(count($UserError)>0){ ?>
                    <div class="alert alert-danger">
                        <?php foreach ($UserError as $error) echo $error."<br>"; ?>
                    </div>
                <?php } ?>
                <h2>Your Data</h2>
                <dl>
                    <dt>Username</dt>
                    <dd><?= $user->username ?></dd>
                    <dt>Email</dt>
                    <dd><?= $user->email ?></dd>
                    <dt>First name</dt>
                    <dd><?= $user->firstname ?></dd>
                    <dt>Last name</dt>
                    <dd><?= $user->lastname ?></dd>
                    <dt>Phone</dt>
                    <dd><?= $user->mobile ?></dd>
                </dl>

            </div>
        </div>
    </div>
<?php stopBinding(); ?>
<?php includeView("frame.php"); ?>