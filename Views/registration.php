<?php SetVal("title","Registration"); ?>
<?php SetVal("active",2); ?>
<?php startBinding("main"); ?>
    <div class="container">
        <div class="row my-3" >
            <div class="col-lg-4 mx-md-auto">
                <?php if(count($UserError)>0){ ?>
                    <div class="alert alert-danger">
                        <?php foreach ($UserError as $error) echo $error."<br>"; ?>
                    </div>
                <?php } ?>
                <h2>Registration</h2>
                <form action="/registration" method="post">
                    <div class="form-group">
                        <label for="username">Username:</label>
                        <input type="text" class="form-control" id="username" name="username" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="email" class="form-control" id="email" name="email" required>
                    </div>
                    <div class="form-group">
                        <label for="fn">First name:</label>
                        <input type="text" class="form-control" id="fn" name="fn" required>
                    </div>
                    <div class="form-group">
                        <label for="ln">Last name:</label>
                        <input type="text" class="form-control" id="ln" name="ln" required>
                    </div>
                    <div class="form-group">
                        <label for="mobile">Mobile Number:</label>
                        <input type="tel" class="form-control" id="mobile" name="mobile" required>
                    </div>
                    <div class="form-group">
                        <label for="password">Password:</label>
                        <input type="password" class="form-control" id="password" name="password" required>
                    </div>
                    <div class="form-group">
                        <label for="repassword">Password Again:</label>
                        <input type="password" class="form-control" id="repassword" name="repassword" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
<?php stopBinding(); ?>
<?php includeView("frame.php"); ?>