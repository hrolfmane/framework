<?php SetVal("title","Login"); ?>
<?php SetVal("active",1); ?>
<?php startBinding("main"); ?>
    <div class="container">
        <div class="row mt-3" >
            <div class="col-lg-4 mx-md-auto">
                <?php if(count($UserError)>0){ ?>
                <div class="alert alert-danger">
                    <?php foreach ($UserError as $error) echo $error."<br>"; ?>
                </div>
                <?php } ?>
                <h2>Login</h2>
                <form action="/login" method="post">
                    <div class="form-group">
                        <label for="username">Username or email:</label>
                        <input type="text" class="form-control" id="username" name="username" required>
                    </div>
                    <div class="form-group">
                        <label for="password">Password:</label>
                        <input type="password" class="form-control" id="password" name="password" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Login</button>
                </form>
            </div>
        </div>
    </div>
<?php stopBinding(); ?>
<?php includeView("frame.php"); ?>