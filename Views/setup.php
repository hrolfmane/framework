<!DOCTYPE html>
<html lang="en">
<head>
    <title>Database Setup</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row mt-3" >
        <div class="col-lg-4 mx-md-auto">
            <?php if(count($UserError)>0){ ?>
                <div class="alert alert-danger">
                    <?php foreach ($UserError as $error) echo $error."<br>"; ?>
                </div>
            <?php } ?>
            <h2>Databse Setup</h2>
            <form action="/setup" method="post">
                <div class="row">
                    <div class="col-8">
                        <div class="form-group">
                            <label for="host">host:</label>
                            <input type="text" class="form-control" id="host" name="host" value="127.0.0.1" required>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="port">port:</label>
                            <input type="number" class="form-control" id="port" name="port" value="3306" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="db">Db name:</label>
                    <input type="text" class="form-control" id="db" name="db" required>
                </div>
                <div class="form-group">
                    <label for="user">User:</label>
                    <input type="text" class="form-control" id="user" name="user" required>
                </div>
                <div class="form-group">
                    <label for="pass">Password:</label>
                    <input type="password" class="form-control" id="pass" name="pass">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>