<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= GetVal("title") ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/">For Test</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainMenu" aria-controls="mainMenu" aria-expanded="false" aria-label="Toggle Main Menu">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="mainMenu">
        <ul class="nav navbar-nav ml-auto">
            <?php if(!(\Model\UserModel::UserIsLoggedIn())){ ?>
            <li class="nav-item">
                <a class="nav-link <?= GetVal("active")==1?"active":"" ?>" href="/login">Login</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= GetVal("active")==2?"active":"" ?>" href="/registration">Registration</a>
            </li>
            <?php }else{ ?>
            <li class="nav-item">
                <a class="nav-link <?= GetVal("active")==3?"active":"" ?>" href="/logout">Logout</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= GetVal("active")==4?"active":"" ?>" href="/data">My Data</a>
            </li>
            <?php } ?>
        </ul>
    </div>
</nav>
<?php makeSingleSlot("main") ;?>
</body>
</html>
