<?php


namespace Model;


use System\Model\DataPacket;
use System\Model\Model;

class UserModel extends Model
{
    /**
     * @var string $username
     * @var string $email
     * @var string $firstname
     * @var string $lastname
     * @var string $mobile
     */
    public $username="",$email="",$firstname="",$lastname="",$mobile="";
    public static function LogInUser(string $loginData,string $password):bool{
        $password=hash('sha3-512',$password);
        $res= Model::ExPrepStmFA("SELECT username, email, firstname, lastname, mobile FROM users WHERE (username= :ld OR email = :ld) AND password = :pa LIMIT 1",array(":ld"=>$loginData,":pa"=>$password));
        if(count($res??array()>0)){
            $user=DataPacket::getDataPacket("user");
            $user->username=$res[0]["username"];
            $user->email=$res[0]["email"];
            $user->firstname=$res[0]["firstname"];
            $user->lastname=$res[0]["lastname"];
            $user->mobile=$res[0]["mobile"];
            return true;
        }
        return false;
    }
    public static function LogOutUser(){
        DataPacket::getDataPacket("user")->clearDataPacket();
    }
    public static function UserIsLoggedIn():bool{
        return !((DataPacket::getDataPacket("user")->isClear()));
    }
    public static function GetLoggedInUser():?UserModel{
        if(UserModel::UserIsLoggedIn()) {
            $ret = new UserModel();
            $user = DataPacket::getDataPacket("user");
            $ret->username = $user->username;
            $ret->email = $user->email;
            $ret->firstname = $user->firstname;
            $ret->lastname = $user->lastname;
            $ret->mobile = $user->mobile;
            return $ret;
        }
        return null;
    }
    public static function CheckUsername(string $username){
        $res= Model::ExPrepStmFA("SELECT username FROM users where username = :us LIMIT 1",array(":us"=>$username));
        if($res===null)return false;
        return (count($res) == 0);
    }
    public static function CheckEmail(string $email){
        $res= Model::ExPrepStmFA("SELECT email FROM users where email = :em LIMIT 1",array(":em"=>$email));
        if($res===null)return false;
        return (count($res) == 0);
    }
    public static function RegisterAndLogInUser(UserModel $user,string $password):bool{
        $password=hash('sha3-512',$password);
        $res= Model::ExPrepStmRC("INSERT INTO users (username, email, firstname, lastname, mobile, password) VALUES (?,?,?,?,?,?)",
            array($user->username,$user->email,$user->firstname,$user->lastname,$user->mobile,$password));
        if($res){
            $userDat = DataPacket::getDataPacket("user");
            $userDat->username = $user->username;
            $userDat->email = $user->email;
            $userDat->firstname = $user->firstname;
            $userDat->lastname = $user->lastname;
            $userDat->mobile = $user->mobile;
            return true;
        }
        return false;
    }
}