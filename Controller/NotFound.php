<?php


namespace Controller;


use System\Controller\Controller;
use System\ResponseType\ResponseType;
use System\View\View;

class NotFound extends Controller
{
    public function __invoke():ResponseType{
        return new View("not_found.php",404);
    }
}