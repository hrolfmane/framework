<?php


namespace Controller;


use System\Controller\Controller;
use System\ResponseType\ResponseType;
use System\View\View;

class MainPage extends Controller
{
    public function __invoke():ResponseType{
        return new View("main.php");
    }
}