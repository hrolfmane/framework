<?php


namespace Controller;


use Model\UserModel;
use System\Controller\Controller;
use System\ErrorHandler\ErrorHandler;
use System\ResponseType\InternalRedirect;
use System\View\View;

class UserController extends Controller
{
    public function login(){
        if($this->request->GetMethod()=="POST") {
            $this->request->AssertVar("username", "you need to give a username or email");
            $this->request->AssertVar("password", "you need to fill the password field");
            if(!ErrorHandler::IsUserError())
            if(UserModel::LogInUser($this->request->username,$this->request->password)){
                return new InternalRedirect("/");
            }else{
                ErrorHandler::AddUserError("incorrect username or password");
            }
        }
        return new View("login.php");
    }
    public function logout(){
        UserModel::LogOutUser();
        return new InternalRedirect("/");
    }
    public function registration(){
        if($this->request->GetMethod()=="POST") {
            $this->request->AssertVar("username", "you have to give a username");
            $this->request->AssertVar("email", "you have to give an email");
            $this->request->AssertVarFormat("email", "^[a-z](?:[a-z][_\-\+\.]?)+[a-z]@[a-z]+\.[a-z]{3,5}$", "incorrect email format");
            $this->request->AssertVar("fn", "you have to give a first name");
            $this->request->AssertVar("ln", "you have to give a last name");
            $this->request->AssertVar("mobile", "you have to give a mobile number");
            $this->request->AssertVarFormat("mobile", "^\d{6,12}$", "incorrect mobile number format");
            $this->request->AssertVar("password", "you have to give a password");
            $this->request->AssertVar("repassword", "you have to repeat the password");
            $this->request->AssertVarEquality("password", "repassword", "the repeated password is incorrect");
            if(!ErrorHandler::IsUserError()){
                if(!UserModel::CheckUsername($this->request->username))ErrorHandler::AddUserError("you can't use this username");
                if(!UserModel::CheckEmail($this->request->email))ErrorHandler::AddUserError("you can't use this email");
                if(!ErrorHandler::IsUserError()){
                    $user = new UserModel();
                    $user->username=$this->request->username;
                    $user->email=$this->request->email;
                    $user->firstname=$this->request->fn;
                    $user->lastname=$this->request->ln;
                    $user->mobile=$this->request->mobile;
                    if(UserModel::RegisterAndLogInUser($user,$this->request->password)){
                        return new InternalRedirect("/");
                    }else{
                        ErrorHandler::AddUserError("registration failed");
                    }
                }
            }
        }
        return new View("registration.php");
    }
    public function userdata(){
        $view = new View("userdata.php");
        $view->user=UserModel::GetLoggedInUser();
        return $view;
    }
}