<?php


namespace Middleware;


use Closure;
use Model\UserModel;
use System\Middleware\Middleware;
use System\Request\Request;
use System\ResponseType\Block;

class LInBlock implements Middleware
{

    public function handle(Request $request, Closure $next)
    {
        if(UserModel::UserIsLoggedIn())return new Block();
        else return $next($request);
    }
}